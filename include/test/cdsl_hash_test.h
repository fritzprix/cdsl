/*
 * cdsl_hash_test.h
 *
 *  Created on: 2015. 8. 29.
 *      Author: innocentevil
 */

#ifndef CDSL_HASH_TEST_H_
#define CDSL_HASH_TEST_H_

#include "cdsl.h"

#ifdef __cplusplus
extern "C" {
#endif

extern BOOL cdsl_hashDoTest(void);


#ifdef __cplusplus
}
#endif

#endif /* CDSL_HASH_TEST_H_ */
